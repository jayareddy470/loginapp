import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class Demo
	{
	 public static void main(String[] args) throws SQLException
	 {
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","SYSTEM_32","system");
			Statement st=con.createStatement();
			
			ResultSet rs=st.executeQuery("SELECT *FROM student");
			ResultSetMetaData rmData=rs.getMetaData();
			int n=rmData.getColumnCount();
			for (int i=1;i<=n;i++)
			{
				System.out.print(rmData.getColumnName(i)+"\t");
			}
			while(rs.next())
			{
				System.out.print(rs.getInt(1)+"\t");
				System.out.print(rs.getString(2)+"\t");
				System.out.print(rs.getInt(3)+"\t");
				System.out.println();	
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	 }
	} 
